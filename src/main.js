import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import createAuth0Client from '@auth0/auth0-spa-js'
import 'buefy/dist/buefy.css'
import PostEdit from "./components/PostEdit";
import HelloWorld from "./components/HelloWorld";
import PostsList from "./components/PostsList";

const axios = require('axios').default;

Vue.config.productionTip = false

Vue.use(Buefy);
Vue.use(VueRouter);
Vue.use(Vuex);

const routes = [
  {path: "/", component: HelloWorld},
  {path:"/posts", component: PostsList},
  {path: "/posts/add", component: PostEdit}
];

const router = new VueRouter({
  routes
});

const store = new Vuex.Store({
  state: {
    posts:[]
  },
  mutations:{
    ADD_POST(state, post) {
      post.id = Math.random();
      state.posts.push(post)
    },
    SET_POSTS(state, posts) {
      state.posts = posts
    }
  },
  getters: {
    getPosts: state => {
      return state.posts
    },
    postsCount: state => {
      return state.posts.length
    }
  },
  actions:{
    async getPosts({commit}){
      const posts = await axios.get("https://jsonplaceholder.typicode.com/posts")
      commit("SET_POSTS", posts.data)
    }
  }
});

createAuth0Client({
  domain: 'lifetec.eu.auth0.com',
  client_id: "0qYYEWbB5Q0k8dNGTJk3M4bM67cO22Ne",
  redirect_url: "http://localhost:8080/"
}).then(client => {
  Vue.prototype.$auth0 = client;
})



new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
